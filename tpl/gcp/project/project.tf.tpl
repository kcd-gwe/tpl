resource "google_project" "{{ (datasource "tf-meta").metadata.project.project_id }}" {
  name       = "{{ (datasource "tf-meta").metadata.project.name }}"
  project_id = "{{ (datasource "tf-meta").metadata.project.project_id }}"
  folder_id     = "{{ (datasource "tf-meta").metadata.project.folder_id }}"
  billing_account= "{{ (datasource "tf-meta").metadata.project.billing_account }}"
}

{{ range $account, $value := (datasource "tf-meta").metadata.serviceaccounts }}
resource "google_service_account" "{{ $account }}" {
  account_id   = "{{ $account }}"
  display_name = "{{ $value.display_name }}"
}
{{ end }}

